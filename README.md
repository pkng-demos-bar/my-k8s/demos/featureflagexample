# FeatureFlagExample

Demo for feature flag

## Containerd build

```bash

nerdctl build --tag featureflag .

nerdctl run -it --rm -p 3000:5000 featureflag:latest

```