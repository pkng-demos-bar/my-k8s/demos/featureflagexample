import sqlite3
import logging
import os
from flask import Flask, render_template, request, url_for, flash, redirect
from flask_unleash import Unleash
from werkzeug.exceptions import abort



def get_db_connection():
    conn = sqlite3.connect('database.db')
    conn.row_factory = sqlite3.Row
    return conn


def get_post(post_id):
    conn = get_db_connection()
    post = conn.execute('SELECT * FROM posts WHERE id = ?',
                        (post_id,)).fetchone()
    conn.close()
    if post is None:
        abort(404)
    return post


app = Flask(__name__)
app.config['SECRET_KEY'] = 'your secret key'

logging.basicConfig()
#logging.getLogger().setLevel(logging.DEBUG)
logging.getLogger('flask_unleash').setLevel(logging.DEBUG)

# Configuration for Flask-Unleash
# https://docs.getunleash.io/Flask-Unleash/

# GitLab unleash configuration
app.config["UNLEASH_URL"] = os.getenv("UNLEASH_URL","https://gitlab.com/api/v4/feature_flags/unleash/46761789")
# GitLab refers this as environment
app.config["UNLEASH_APP_NAME"] = os.getenv("GITLAB_ENVIRONMENT_NAME","production")
app.config["UNLEASH_INSTANCE_ID"] = os.getenv("UNLEASH_INSTANCE_ID","snZL-EoM4zPzsz5x3fDr")

app.config["UNLEASH_ENVIRONMENT"] = ""

# GitLab does not support registration and metrics
app.config["UNLEASH_DISABLE_REGISTRATION"] = True
app.config["UNLEASH_DISABLE_METRICS"] = True

app.config["UNLEASH_REFRESH_INTERVAL"] = "15"

unleash = Unleash(app)

@app.route('/')
def index():
    conn = get_db_connection()
    posts = conn.execute('SELECT * FROM posts').fetchall()
    conn.close()

    flag_allow_new_post = unleash.client.is_enabled("newpost")
    logging.debug(f"Got feature flag value of: {flag_allow_new_post}")

    return render_template('index.html', posts=posts, create=flag_allow_new_post)


@app.route('/<int:post_id>')
def post(post_id):
    post = get_post(post_id)
    return render_template('post.html', post=post)


@app.route('/create', methods=('GET', 'POST'))
def create():

    if request.method == 'POST':
        title = request.form['title']
        content = request.form['content']

        if not title:
            flash('Title is required!')
        else:
            conn = get_db_connection()
            conn.execute('INSERT INTO posts (title, content) VALUES (?, ?)',
                         (title, content))
            conn.commit()
            conn.close()
            return redirect(url_for('index'))

    return render_template('create.html')


@app.route('/<int:id>/edit', methods=('GET', 'POST'))
def edit(id):
    post = get_post(id)

    if request.method == 'POST':
        title = request.form['title']
        content = request.form['content']

        if not title:
            flash('Title is required!')
        else:
            conn = get_db_connection()
            conn.execute('UPDATE posts SET title = ?, content = ?'
                         ' WHERE id = ?',
                         (title, content, id))
            conn.commit()
            conn.close()
            return redirect(url_for('index'))

    return render_template('edit.html', post=post)


@app.route('/<int:id>/delete', methods=('POST',))
def delete(id):
    post = get_post(id)
    conn = get_db_connection()
    conn.execute('DELETE FROM posts WHERE id = ?', (id,))
    conn.commit()
    conn.close()
    flash('"{}" was successfully deleted!'.format(post['title']))
    return redirect(url_for('index'))
