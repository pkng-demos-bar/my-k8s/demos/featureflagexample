# Using official python runtime base image
FROM python:3.9-slim

# add curl for healthcheck
RUN apt-get update \
    && apt-get install -y --no-install-recommends \
    curl \
    && rm -rf /var/lib/apt/lists/*

# Set the application directory
WORKDIR /app

# Install our requirements.txt
COPY requirements.txt /app/requirements.txt
RUN pip3 install -r requirements.txt

# Copy our code from the current folder to /app inside the container
COPY . .

# initialize the default database
RUN python3 init_db.py

# Make port 5000 available for links and/or publish
ENV PORT 5000
EXPOSE $PORT

# Define our command to be run when launching the container
CMD ["python3", "-m", "flask", "run", "--host=0.0.0.0", "--debug"]
